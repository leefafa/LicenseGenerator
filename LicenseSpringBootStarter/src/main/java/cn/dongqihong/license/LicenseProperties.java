package cn.dongqihong.license;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * Todo 请添加类描述
 *
 * @author DongQihong
 * @since 2021/8/3
 */
@ConfigurationProperties(prefix = "spring.license")
public class LicenseProperties {
    private String licPath = "license.txt";
    private String pubKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKaJwexOQW2B4bcdrgqP1zZsKyqL2pmwPafH0Q3d2ogl4lZTEOOqAJeoG0ufDcPLog5j/Q1qr+AWmWDWwrUUdLsCAwEAAQ==";

    public String getLicPath() {
        return licPath;
    }

    public void setLicPath(String licPath) {
        this.licPath = licPath;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }
}
