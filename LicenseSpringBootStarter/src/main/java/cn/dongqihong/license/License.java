package cn.dongqihong.license;

import java.util.Date;

/**
 * 授权
 * 产品序列号    :  0000000000
 * 产品名称      :  某某系统
 * 产品使用人    :  某某公司
 * 授权截止日期  :  2022-07-30
 *
 * @author DongQihong
 * @since 2021/8/3
 */
public class License {
    /**
     * 产品名
     */
    private String productName;
    /**
     * 产品序列号
     */
    private String productSerialNumber;
    /**
     * 产品使用人
     */
    private String productUser;
    /**
     * 有效期
     */
    private Date expirationDate;
    /**
     * 是否有效
     */
    private boolean isValid = false;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSerialNumber() {
        return productSerialNumber;
    }

    public void setProductSerialNumber(String productSerialNumber) {
        this.productSerialNumber = productSerialNumber;
    }

    public String getProductUser() {
        return productUser;
    }

    public void setProductUser(String productUser) {
        this.productUser = productUser;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
